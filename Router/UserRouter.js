const express = require('express');
const captchapng = require('captchapng');
const fs = require("fs");
const jwt = require("jsonwebtoken");
const privateKey = "zhang101";
const bcrypt = require("bcrypt");
const baseConfig = require("../baseConfig.js");
const {User} = require('../DB_config/Schemas/user.js');
const Srandom = require("string-random");
const router = express.Router();

// 用户账号注册
router.post('/userRegist', async function (req, res, next) {
    console.log(req.body)
    const {token, code} = req.body;
    let user = {};
    let userName = {};
    let userPhone = {};
    const hashHedPassword = await bcrypt.hash(req.body.passwd, 10);  //加密方式、次数
    userName.userName = req.body.name;
    userPhone.userPhone = req.body.phone;
    user.userName = req.body.name;
    user.userPassword = hashHedPassword;
    user.userPhone = req.body.phone;
    user.userRegDate = new Date();
    const decoded = jwt.verify(token, privateKey);
    if (decoded && decoded.iss === code) {
        if (await User.findOne({where: userName})) {
            res.json({code: 1, msg: "用户名已经存在！请更换用户名后重试！"})
        } else {
            if (await User.findOne({where: userPhone})) {
                res.json({code: 1, msg: "手机号已经存在！请更换手机号后重试！"})
            } else {
                try {
                    await User.create(user);
                    res.json({code: 0, msg: 'regist success!'});
                } catch (e) {
                    res.status(500).json({code: 1, msg: e.message});
                }
            }
        }
    } else {
        res.json({code: 1, msg: "验证码错误！"});
    }
});

//获取用户信息
router.post('/getInfo', async (req, res, next) => {
    let info = {};
    // let {id} = req.body.id;
    let {id} = req.body;
    info.userId = id;
    try {
        await User.findOne({where: info}).then(result => {
            res.json({code: 0, msg: '查询成功！', data: result});
        });
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});
//用户手机号注册
router.post("/userPhoneRegist", async (req, res, next) => {
    const {token, code} = req.body;
    let userPhone = {};
    let userName = {};
    let user = {};
    let defaultPassword = "a" + req.body.phone_num;
    const hashHedPassword = await bcrypt.hash(defaultPassword, 10);  //加密方式、次数
    userPhone.userPhone = req.body.phone_num;
    user.userName = Srandom(6, {numbers: false});   //生成用户名
    userName.userName = user.userName;
    user.userPhone = req.body.phone_num;
    user.userPassword = hashHedPassword;
    user.userRegDate = new Date().getDay();
    const decoded = jwt.verify(token, privateKey);
    if (decoded && decoded.iss === code) {
        if (await User.findOne({where: userName})) {
            res.json({code: 1, msg: "注册失败，请重试！"});
            return false
        } else {
            if (await User.findOne({where: userPhone})) {
                res.json({code: 1, msg: "手机号已经存在！请更换手机号后重试！"});
                return false
            } else {
                try {
                    await User.create(user);
                    res.json({
                        code: 0,
                        msg: `恭喜你，注册成功！`,
                        userName: user.userName,
                        password: defaultPassword,
                        tip: '请用手机号登陆！'
                    });
                } catch (e) {
                    res.status(500).json({code: 1, msg: e.message});
                }
            }
        }
    } else {
        res.json({code: 1, msg: "验证码错误！"});
    }
});

//用户找回密码
router.post('/userFindPassword', async (req, res, next) => {
    const {token, code, comfirm_new_password} = req.body;
    let userPhone = {};
    let updateInfo = {};
    const hashHedPassword = await bcrypt.hash(comfirm_new_password, 10);
    userPhone.userPhone = req.body.phone;
    updateInfo.userPassword = hashHedPassword;
    try {
        const decoded = jwt.verify(token, privateKey);
        if (decoded && decoded.iss === code) {
            if (await User.update(updateInfo, {where: userPhone})) {
                res.json({code: 0, msg: "修改密码成功！"});
            } else {
                res.json({code: 1, msg: "修改密码失败！用户不存在！"});
            }
        } else {
            res.json({code: 1, msg: "验证码错误！"});
        }
    } catch (e) {
        res.json({code: 1, msg: e.message});
    }
});

//用户修改密码
router.post('/modifyPassword', async (req, res, next) => {
    let findData = {};
    let UpdateData = {};
    let {oldPassword, newPassword, comfirmPassword, userID} = req.body;
    const hashHedPassword = await bcrypt.hash(comfirmPassword, 10);
    findData.userID = userID;
    UpdateData.userPassword = hashHedPassword;
    try {
        let result = await User.findOne({where: findData});
        if (await bcrypt.compare(oldPassword, result.userPassword)) {
            await User.update(UpdateData, {where: findData}).then(result => {
                res.json({code: 0, msg: '修改密码成功！'})
            });
        } else {
            res.json({code: 1, msg: '旧密码不一样'})
        }
    } catch (e) {
        res.json({code: 1, msg: e.message});
    }
});

//用户修改信息
router.post('/updateInfo', async (req, res, next) => {
    // console.log(req.body)
    let updateInfo = {};
    updateInfo.userId = req.body.userId;
    updateInfo.userName = req.body.userName;
    updateInfo.userPhone = req.body.userPhone;
    updateInfo.userSex = req.body.userSex;
    updateInfo.userAvator = req.body.userAvator;
    updateInfo.userEmail = req.body.userEmail;
    updateInfo.userBirthday = req.body.userBirthday;
    updateInfo.userStatement = req.body.userStatement;
    try {
        await User.update(updateInfo, {where: {userId:req.body.userId}}).then(result => {
            User.findOne( {where: {userId:req.body.userId}}).then(result1 =>{
                result1.userPassword ='';
                res.json({code: 0, msg: '修改成功！', data: result1})
            });

        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

// 图片验证验证码及用户登录
router.post('/userLogin', async function (req, res, next) {
    const {body} = req;
    const {token, code} = body;
    const userinfo = {
        userName: req.body.name,
    };
    const userPhone = {
        userPhone: req.body.name,
    };
    try {
        const decoded = jwt.verify(token, privateKey);
        if (decoded && decoded.iss === code) {
            //添加数据库操作
            const result = await User.findOne({where: userinfo});
            let result1 = await User.findOne({where: userPhone});
            if (result != null) {
                if (await bcrypt.compare(req.body.passwd, result.userPassword)) {
                    // res.cookie('username',result.userName,{maxAge:3600000});  //设置cookie
                    res.cookie('username', result.userName);
                    res.cookie('userid', result.userID);
                    res.cookie('useravatar', result.userAvator);
                    result.userPassword = '';
                    res.json({code: 0, msg: "登陆成功！", user: result});
                } else {
                    res.json({code: 1, msg: "密码不一致！"});
                }
            } else if (result1 != null) {
                if (await bcrypt.compare(req.body.passwd, result1.userPassword)) {
                    // res.cookie('username',result.userName,{maxAge:3600000});  //设置cookie
                    res.cookie('username', result1.userName);
                    result1.userPassword = '';
                    res.json({code: 0, msg: "登陆成功！", user: result1});
                } else {
                    res.json({code: 1, msg: "密码不一致！"});
                }
            } else {
                res.json({code: 1, msg: "登陆失败！"});
            }
            return;
        }
        res.json({code: 1, msg: "验证码错误！"});
    } catch (err) {
        res.json({code: 1, msg: err.message});
    }
});

//手机号码登录
router.post("/userPhoneLogin", async (req, res, next) => {
    const {body} = req;
    const {token, code} = body;
    const userinfo = {
        userPhone: req.body.phone_num,
    };
    try {
        const decoded = jwt.verify(token, privateKey);
        if (decoded && decoded.iss === code) {
            //添加数据库操作
            const result = await User.findOne({where: userinfo});
            if (result != null) {
                // res.cookie('username',result.userName,{maxAge:3600000});  //设置cookie
                res.cookie('username', result.userName);
                res.cookie('userid', result.userID);
                res.cookie('useravatar', result.userAvator);
                result.userPassword = '';
                res.json({code: 0, msg: "登陆成功！", user: result});
            } else {
                res.json({code: 1, msg: "用户未注册,登陆失败！"});
            }
            return;
        }
        res.json({code: 1, msg: "验证码错误！"});
    } catch (err) {
        res.json({code: 1, msg: err.message});
    }
});

//文本验证码生成
router.get('/getNumberCode', (req, res, next) => {
    const code = (Math.random() * 1000000).toString().substr(0, 5);
    const token = jwt.sign({iss: code,}, privateKey);
    res.json({
        code: 0,
        token,
        VerifyCode: code
    });
});

//图片登录验证码生成
router.get("/getCode", (req, res) => {
    const code = (Math.random() * 1000000).toString().substr(0, 4);
    const token = jwt.sign({iss: code,}, privateKey);
    const captcha = new captchapng(80, 30, code);
    captcha.color(0, 0, 0, 0);
    captcha.color(80, 80, 80, 255);
    const img = captcha.getBase64();
    const imgbase64 = Buffer.from(img, 'base64');
    //这个是必须是绝对路径
    fs.writeFileSync(baseConfig.captcha, imgbase64);
    res.json({
        code: 0,
        token,
        imgUrl: `${baseConfig.baseUrl}/images/captcha.png`    //图片地址
    });
});

module.exports = router;