const express = require('express');
const router = express.Router();
const {Article} = require('../DB_config/Schemas/article.js');
const {Video} = require("../DB_config/Schemas/SCVideo.js");
const {ArtCg} = require('../DB_config/Schemas/bs_article_caogao.js');
const {User} = require('../DB_config/Schemas/user.js');
const dt = require('silly-datetime');

// 发布接口
router.post('/article/release', function (req, res, next) {
    let article = {};
    article = req.body;
    article.releaseTime = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    article.updateTime = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    article.isRelease = 1;
    article.isRefuse = 0;
    article.isVerify = 0;
    article.isPassVerify = 0;
    try {
        Article.create(article).then(result => {
            res.json({code: '0', msg: 'success'})
        })
    } catch (e) {
        res.json({code: '1', msg: '插入失败,' + e.message})
    }
});

//修改接口
router.post('/article/modify', function (req, res, next) {
    let article = {};
    let find = {};
    find.id = req.body.id;
    find.userId = req.body.userId;
    find.sectionName = req.body.sectionName;
    article = req.body;
    article.releaseTime = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    article.updateTime = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    article.isRelease = 1;
    article.isRefuse = 0;
    article.isVerify = 0;
    article.isPassVerify = 0;
    try {
        Article.update(article, {where: find}).then(result => {
            res.json({code: '0', msg: 'success'})
        })
    } catch (e) {
        res.json({code: '1', msg: '插入失败,' + e.message})
    }
});

//查看文章详情
router.post('/article/detail', (req, res, next) => {
    let {id} = req.body;
    let info = {};
    info.id = id;
    Article.belongsTo(User, {foreignKey: 'userID', targetKey: 'userID'});   //联表查询
    try {
        Article.findOne({
            include: {
                model: User,
                attributes: ['userStatement', 'userName', "userAvator"],
            },
            where: info
        }).then(result => {
            res.json({code: 0, msg: '查询成功！', data: result});
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

// 存草稿箱
router.post('/article/caogao', function (req, res, next) {
    let caogao = {...req.body};
    // caogao = req.body;
    caogao.releaseTime = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    caogao.updateTime = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    caogao.isRelease = 0;
    caogao.isRefuse = 0;
    caogao.isVerify = 0;
    caogao.isPassVerify = 0;
    try {
        ArtCg.create(caogao).then(result => {
            res.json({code: '0', msg: '保存草稿成功！'})
        })
    } catch (e) {
        res.json({code: '1', msg: '保存草稿失败！' + e.message})
    }
});

//修改稿子
router.post('/article/draft/modify',async (req,res,next)=>{
    let caogao = {...req.body};
    let find = {};
    find.id = req.body.id;
    find.userId = req.body.userId;
    find.sectionName = req.body.sectionName;
    // caogao = req.body;
    caogao.releaseTime = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');  //不修改发布时间
    caogao.updateTime = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    caogao.isRelease = 0;
    caogao.isRefuse = 0;
    caogao.isPassVerify =0;
    try {
        ArtCg.update(caogao,{where:find}).then(result => {
            res.json({code: '0', msg: '修改草稿成功！'})
        })
    } catch (e) {
        res.json({code: '1', msg: '修改草稿失败！' + e.message})
    }
});

// /草稿箱转发布
router.post('/article/caogao/to_release', async (req, res, next) => {
    let release = {...req.body};
    let {id} = req.body;
    release.id = '';
    release.releaseTime = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    release.updateTime = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    release.isRelease = 1;
    release.isRefuse = 0;
    release.isPassVerify = 0;   //默认没有通过审核
    try{
        await ArtCg.destroy({where:{id: id}});   //删除草稿箱的内容
        await Article.create(release).then(result =>{
            res.json({code:0, msg: '发布草稿成功！'})
        })
    }catch (e) {
        res.json({code:1, msg: '草稿发表失败！' + e.message})
    }
});

//草稿删除
router.post('/article/delete_caogao',async (req,res,next)=>{
    let {id} =req.body;
    try{
       await ArtCg.destroy({where:{id:id}}).then(success=>{
            res.json({code:0,msg:'删除成功！'})
        })
    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});
//获取草稿详情
router.post('/article/caogao/detail',async (req,res,next)=>{
    let {id} = req.body;
    try{
        await ArtCg.findOne({where:{id:id}}).then(success=>{
            res.json({code:0,msg:'获取草稿成功！',data:success})
        })
    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});

//获取文章
router.post('/article/getArticle', async (req, res, next) => {
    let {size,page} = req.body;
    let find_limit_data = {};   //限制条件
    find_limit_data.isDelete = 0;
    find_limit_data.isPassVerify = 1;
    try {
        let maxlenth = await Article.findAll();
        if(size <=10&& page ==1){
            let result = await Article.findAll({where:find_limit_data,limit: 5});
            res.json({code: 0, msg: '获取文章列表成功！', data: result, length: maxlenth.length});
        }else{
            let result = await Article.findAll({where:find_limit_data,limit: size,offset:(page-1)*size});
            res.json({code: 0, msg: '获取文章列表成功！', data: result, length: maxlenth.length});
        }

    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//主页获取数据视频和文章
router.post('/getData', async (req, res, next) => {
    let {size,page} = req.body;
    let find_limit_data = {};    //限制条件
    find_limit_data.isDelete = 0;
    find_limit_data.isPassVerify = 1;
    try {
        let video1 = await Video.findAll();
        let article1 = await Article.findAll();
        if (size<=5 && page == 1){
            let video = await Video.findAll({where:find_limit_data,limit: 5});
            let article = await Article.findAll({where:find_limit_data,limit: 5});
            video.push(...article);
            res.json({code: 0, msg: '获取数据成功！', data: video, length: video1.length + article1.length});
        }else{
            let video = await Video.findAll({where:find_limit_data,limit: size,offset:size*(page-1)});
            let article = await Article.findAll({where:find_limit_data,limit: size,offset:size*(page-1)});
            video.push(...article);
            res.json({code: 0, msg: '获取数据成功！', data: video, length: video1.length + article1.length});
        }
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//删除审核中的文章 /article/delete
router.post('/article/delete',async (req,res,next)=>{
    let {id} = req.body;
    let updateData = {...req.body};
    updateData.isDelete = 1;
    try{
        await Article.update(updateData,{where:{id:id}}).then(success=>{
            res.json({code:0,msg:'success delete！'});
        })
    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});
module.exports = router;