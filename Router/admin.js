const express = require('express');
const router = express.Router();
const {Comment} = require('../DB_config/Schemas/comment.js');
const {Article} = require('../DB_config/Schemas/article');
const {Video} = require('../DB_config/Schemas/SCVideo.js');
const {User} = require('../DB_config/Schemas/user.js');
const {Admin} = require('../DB_config/Schemas/admin.js');
const dt = require('silly-datetime');

//管理数据
router.get('/getAllData', async (req, res, next) => {
    let verify_limit = {};
    verify_limit.isPassVerify = 0;
    verify_limit.isDelete = 0;
    let limit_normal = {};
    limit_normal.isPassVerify = 1;
    limit_normal.isDelete = 0;
    Comment.belongsTo(Article, {foreignKey: 'ObjectID', targetKey: 'id'});
    Comment.belongsTo(Video, {foreignKey: 'ObjectID', targetKey: 'id'});
    try {
        let super_user = await Admin.findAll();
        let verify_article = await Article.findAll({where: verify_limit});
        let verify_video = await Video.findAll({where: verify_limit});
        let verify_comment = await Comment.findAll({where: {isPassverify: 0}});
        let users = await User.findAll();
        let video = await Video.findAll({where: limit_normal});
        let article = await Article.findAll({where: limit_normal});
        let comment = await Comment.findAll({where: {isPassverify: 1}});
        res.json({
            code: 0,
            msg: "获取数据成功！",
            article,
            video,
            comment,
            super_user,
            users,
            verify_article,
            verify_video,
            verify_comment
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});
//板块数据 =>charts
router.get('/getSectionData', async (req, res, next) => {
    let te = {};
    let live = {};
    let study = {};
    live.sectionName = '生活区';
    te.sectionName = '技术区';
    study.sectionName = "学习区";
    try {
        let video_section_te = await Video.findAll({where: te});
        let video_section_live = await Video.findAll({where: live});
        let video_section_study = await Video.findAll({where: study});
        let article_section_te = await Article.findAll({where: te});
        let article_section_live = await Article.findAll({where: live});
        let article_section_study = await Article.findAll({where: study});
        let te_data = [];
        let live_data = [];
        let study_data = [];
        te_data.push(...article_section_te);
        te_data.push(...video_section_te);
        live_data.push(...video_section_live);
        live_data.push(...article_section_live);
        study_data.push(...article_section_study);
        study_data.push(...video_section_study);
        res.json({code: 0, msg: "sucess", te_data, live_data, study_data});
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }

});

// admin获取评论的稿子信息
router.post('/getData', async (req, res, next) => {
    let {ObjectID, type} = req.body;
    try {
        if (type == 'V') {
            let data = await Video.findOne({where: {id: ObjectID}});
            res.json({code: 0, msg: 'success', data: data})
        } else {
            let data = await Article.findOne({where: {id: ObjectID}});
            res.json({code: 0, msg: 'success', data: data})
        }
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//verify comment
router.post('/verifyComment', async (req, res, next) => {
    let data = {};
    data.commentID = req.body.commentID;
    data.isPassverify = 1;
    console.log(data);
    try {
        await Comment.update(data, {where: {commentID: data.commentID}}).then(result => {
            res.json({code: 0, msg: 'success'})
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//verify video
router.post('/verifyVideo', async (req, res, next) => {
    let data = {...req.body};
    // data.isPassVerify = 1;
    try {
        await Video.update(data, {where: {id: data.id}}).then(result => {
            res.json({code: 0, msg: 'success'})
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//verify Article
router.post('/verifyArticle', async (req, res, next) => {
    let data = {...req.body};
    // data.isPassVerify = 1;
    try {
        await Article.update(data, {where: {id: data.id}}).then(result => {
            res.json({code: 0, msg: 'success'})
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//back article
router.post('/backArticle', async (req, res, next) => {
    let data = {...req.body};
    data.isRefuce = 1;
    try {
        await Article.update(data, {where: {id: data.id}}).then(result => {
            res.json({code: 0, msg: 'back success'})
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//back Video
router.post('/backVideo', async (req, res, next) => {
    let data = {...req.body};
    data.isRefuse = 1;
    try {
        await Video.update(data, {where: {id: data.id}}).then(result => {
            res.json({code: 0, msg: 'back success'})
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//limit user
router.post('/limitUser', async (req, res, next) => {
    let data = {...req.body};
    data.isLimit = 1;
    try {
        await User.update(data, {where: {userId: req.body.userID}}).then(result => {
            res.json({code: 0, msg: '封号成功！'})
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//管理员删除用户
router.post('/deleteUser', async (req, res, next) => {
    let data = {...req.body};
    try {
        await User.destroy({where: data}).then(result => {
            res.json({code: 0, msg: '删除用户成功！'})
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//管理员添加管理员
router.post('/addAdmin', async (req, res, next) => {
    let data = {...req.body};
    let data1 = {};
    data1.adminName = data.name;
    data1.adminPhone = data.phone;
    data1.adminPassword = data.passwd;
    try {
        if (await Admin.findOne({where: {adminName: data.name}})) {
            res.json({code: 1, msg: "管理员已存在！"})
        } else {
            await Admin.create(data1);
            res.json({code: 0, msg: "添加成功！"})
        }
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//admin update info
router.post('/adminUpdateInfo', async (req, res, next) => {
    let data = {...req.body};
    try {
        if (await Admin.findOne({where: {adminName: data.adminName}})) {
            res.json({code: 1, msg: "管理员已存在！"})
        } else {
            await Admin.update(data, {where: {adminID: data.adminID}});
            res.json({code: 0, msg: "修改成功！"})
        }

    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});
//admin delete info
router.post('/deleteAdmin', async (req, res, next) => {
    let data = {...req.body};
    try {
        let result = await Admin.findAll();
        if (result.length == 1) {
            res.json({code: 1, msg: "不能把管理员都删除！"})
        } else {
            await Admin.destroy({where: {adminID: data.adminID}});
            res.json({code: 0, msg: "删除成功！"})
        }

    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});


module.exports = router;