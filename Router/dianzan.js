const express = require('express');
const router = express.Router();
const {Comment} = require('../DB_config/Schemas/comment.js');
const {User} = require('../DB_config/Schemas/user.js');
const {Dianzan} = require('../DB_config/Schemas/dianzan.js');
const dt = require('silly-datetime');


//点赞记录
router.post('/AgreeSave', async (req, res, next) => {
    let data = {};
    let {articleID, userid, AgreetTime, type} = req.body;
    data.ObjectID = articleID;
    data.userID = userid;
    data.type = type;
    data.AgreeTime = AgreetTime;
    try {
        if(await Dianzan.findOne({where:{ObjectID:data.ObjectID,type:data.type,userID:data.userID}})){
            res.json({code: 1, msg: '你已经点赞过了，换个试试吧！'})
        }else{
            await Dianzan.create(data).then(result => {
                res.json({code: 0, msg: '点赞成功！'})
            })
        }

    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//点赞数目获取
router.post('/getAgree', async (req, res, next) => {
    console.log(req.body);
    let data = {};
    let {type, articleID} = req.body;
    data.type = type;
    data.ObjectID = articleID;
    try {
        await Dianzan.findAll({
            where: data
        }).then(result => {
            res.json({code: 0, msg: '获取成功！', data: result});
        })
    } catch (e) {
        res.json({code: 1, msg: e.message});
    }
});


module.exports = router;