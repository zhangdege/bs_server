const express = require('express');
const router = express.Router();
const {Comment} = require('../DB_config/Schemas/comment.js');
const {User} = require('../DB_config/Schemas/user.js');
const dt = require('silly-datetime');


//保存文章评论
router.post('/save', async (req, res, next) => {
    let data = {};
    data.ObjectID = req.body.articleID;
    data.userID = req.body.userid;
    data.CommentTime = req.body.CommentTime;
    data.conment_centent = req.body.content;
    data.type = req.body.type;
    try {
        await Comment.create(data).then(result => {
            res.json({code: 0, msg: "评论成功！"})
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//保存视频评论
router.post('/VideoSave', async (req, res, next) => {
    let data = {};
    data.ObjectID = req.body.articleID;
    data.userID = req.body.userid;
    data.CommentTime = req.body.CommentTime;
    data.conment_centent = req.body.content;
    data.type = req.body.type;
    try {
        await Comment.create(data).then(result => {
            res.json({code: 0, msg: "评论成功！"})
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});


//获取文章评论
router.post('/getComment', async (req, res, next) => {
    let userinfo = {};
    let articleinfo = {};
    articleinfo.ObjectID = req.body.articleid;
    articleinfo.type = req.body.type;
    Comment.belongsTo(User, {foreignKey: 'userID', targetKey: 'userID'});
    try {
        Comment.findAll({
            include: {
                model: User,
                attributes:['userAvator','userName'],
                // where: userinfo,
            },
            where:articleinfo
        }).then(result => {
            res.json({code:0,msg:"查询成功!",data:result});
        })
    } catch (e) {
        res.json({code:1,msg:e.message})
    }
});

//获取视频评论
router.post('/getVideoComment', async (req, res, next) => {
    let userinfo = {};
    let articleinfo = {};
    userinfo.userID = req.body.userid;
    articleinfo.ObjectID = req.body.articleid;
    articleinfo.type = req.body.type;
    Comment.belongsTo(User, {foreignKey: 'userID', targetKey: 'userID'});
    try {
        Comment.findAll({
            include: {
                model: User,
                attributes:['userAvator','userName'],
                // where: userinfo,
            },
            where:articleinfo
        }).then(result => {
            res.json({code:0,msg:"查询成功!",data:result});
        })
    } catch (e) {
        res.json({code:1,msg:e.message})
    }
});

//删除评论
router.post('/delete',async (req,res,next)=>{
    let {id} = req.body;  //id是唯一的
    try{
        await Comment.destroy({where:{commentID:id}}).then(success =>{
            res.json({code:0,msg:'success'});
        })
    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});

module.exports = router;