const express = require('express');
const router = express.Router();
const {Comment} = require('../DB_config/Schemas/comment.js');
const {User} = require('../DB_config/Schemas/user.js');
const {DisAgree} = require('../DB_config/Schemas/disagree.js');
const dt = require('silly-datetime');


//点赞记录
router.post('/disAgreeSave', async (req, res, next) => {
    let data = {};
    let {articleID, userid, AgreetTime, type} = req.body;
    data.ObjectID = articleID;
    data.userID = userid;
    data.type = type;
    data.disAgreeTime = AgreetTime;
    try {
        if(await DisAgree.findOne({where:{ObjectID:data.ObjectID,type:data.type,userID:data.userID}})){
            res.json({code: 1, msg: '你已经踩过了！'})
        }else{
            await DisAgree.create(data).then(result => {
                res.json({code: 0, msg: '踩！'})
            })
        }
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//点赞数目获取
router.post('/getDisAgree', async (req, res, next) => {
    let data = {};
    let {type, articleID} = req.body;
    data.type = type;
    data.ObjectID = articleID;
    try {
        await DisAgree.findAll({
            where: data
        }).then(result => {
            res.json({code: 0, msg: '获取成功！', data: result});
        })
    } catch (e) {
        res.json({code: 1, msg: e.message});
    }
});


module.exports = router;