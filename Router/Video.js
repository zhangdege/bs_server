const express = require('express');
const router = express.Router();
const multer = require("multer");
const baseConfig = require('../baseConfig.js');
const dt = require('silly-datetime');
const {Video} = require("../DB_config/Schemas/SCVideo.js");
const {User} = require("../DB_config/Schemas/user.js");

// 使用multer对上传图片进行处理
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './video/')
    },
    filename: (req, file, cb) => {
        // console.log('filter',file)
        cb(null, Date.now() + "-" + file.originalname)
    }
});

const upload = multer({
    storage: storage
});

//视频上传接口
router.post('/upload', upload.single("video"), (req, res, next) => {
    // console.log('upload',req.file.filename)
    try {
        let URL = `${baseConfig.video}${req.file.filename}`;
        let filename = req.file.filename;
        let str = {
            success: 1,
            message: "上传成功",
            url: URL,
            filename: filename
        };
        res.json(str)
    } catch (err) {
        let str = {
            success: 0,
            message: "上传失败" + err
        };
        res.json(str)
    }
});

//发布视频接口
router.post('/release', async (req, res, next) => {
    console.log(req.body);
    let time = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    let videoInfo = {};
    videoInfo.title = req.body.title;
    videoInfo.userName = req.body.userName;
    videoInfo.userId = req.body.userId;
    videoInfo.videoUrl = req.body.videoURL;
    videoInfo.pictureurl = req.body.pictureUrl;
    videoInfo.videoName = req.body.videoName;
    videoInfo.pictureName = req.body.pictureName;
    videoInfo.attribute = req.body.attribute;
    videoInfo.sectionName = req.body.sectionName;
    videoInfo.releaseTime = time;
    videoInfo.modifyTime = time;
    videoInfo.isRefuse = 0;
    videoInfo.isPassVerify = 0;
    try {
        await Video.create(videoInfo).then(result => {
            res.json({code: '0', msg: "成功！"});
        })
    } catch (e) {
        res.json({code: '1', msg: e.message})
    }
});

//修改接口
router.post('/modify', async (req, res, next) => {
    console.log(req.body);
    let time = dt.format(Date.now(), 'YYYY-MM-DD HH:mm:ss');
    let videoInfo = {};
    let find = {};
    find.userId = req.body.userId;
    find.id = req.body.id;
    videoInfo.title = req.body.title;
    videoInfo.userName = req.body.userName;
    videoInfo.userId = req.body.userId;
    videoInfo.videoUrl = req.body.videoURL;
    videoInfo.pictureurl = req.body.pictureUrl;
    videoInfo.videoName = req.body.videoName;
    videoInfo.pictureName = req.body.pictureName;
    videoInfo.attribute = req.body.attribute;
    videoInfo.sectionName = req.body.sectionName;
    videoInfo.releaseTime = time;
    videoInfo.modifyTime = time;
    videoInfo.isRefuse = 0;
    videoInfo.isPassVerify = 0;
    try {
        await Video.update(videoInfo,{where:find}).then(result => {
            res.json({code: '0', msg: "成功！"});
        })
    } catch (e) {
        res.json({code: '1', msg: e.message})
    }
});

//查看详情页
router.post('/detail', async (req, res, next) => {
    let {id} = req.body;
    let info = {};
    info.id = id;
    Video.belongsTo(User, {foreignKey: 'userID', targetKey: 'userID'});
    try {
        await Video.findOne({
            include: {
                model: User,
                attributes:['userStatement','userName',"userAvator"],
            },
            where: info
        }).then(result => {
            res.json({code: 0, msg: '查询成功！', result});
        })
    } catch (e) {
        res.json({code: 1, msg: e.message});
    }
});

//获取视频接口
router.post('/getVideo',async (req,res,next)=>{
    let {size,page} = req.body;
    let find_limit_data = {};   //限制条件
    find_limit_data.isDelete = 0;
    find_limit_data.isPassVerify = 1;
    try{
        let maxlenth =await Video.findAll();
        if(size <=5 && page ==1){
            let result = await Video.findAll({where:find_limit_data,limit:size});
            res.json({code:0,msg:'获取视频列表成功！',data:result,length:maxlenth.length});
        }else{
            let result = await Video.findAll({where:find_limit_data,limit:size,offset:(page-1)*size});
            res.json({code:0,msg:'获取视频列表成功！',data:result,length:maxlenth.length});
        }

    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});

//删除审核中的视频 /video/del_video
router.post('/delete',async (req,res,next)=>{
    let {id} = req.body;
    let updateData = {...req.body};
    updateData.isDelete = 1;
    try{
        await Video.update(updateData,{where:{id:id}}).then(success=>{
            res.json({code:0,msg:'success delete！'});
        })
    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});
module.exports = router;