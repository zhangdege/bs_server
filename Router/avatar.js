const express = require('express');
const router = express.Router();
const multer = require("multer");
const baseConfig = require('../baseConfig.js');

// 使用multer对上传图片进行处理
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './avatar/')
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + "-" + file.originalname)
    }
});

const upload = multer({
    storage: storage
});

router.post('/upload', upload.single("avatar"), (req, res, next) => {
    try {
        let URL = `${baseConfig.avatar}${req.file.filename}`;
        let filename = req.file.filename;
        let str = {
            success: 1,
            message: "上传成功",
            url: URL,
            filename: filename
        };
        res.json(str)
    } catch (err) {
        let str = {
            success: 0,
            message: "上传失败" + err
        };
        res.json(str)
    }
});

module.exports = router;