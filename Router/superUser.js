const express = require('express');
const router = express.Router();
const jwt = require("jsonwebtoken");
const privateKey = "zhang101";
const {Admin} = require("../DB_config/Schemas/admin.js");

//管理员登录
router.post('/superUserLogin', function(req, res, next) {
  console.log(req.body);
  const { token, code } = req.body;
  const userinfo = {};
    userinfo.adminName = req.body.name;
  try {
    const decoded = jwt.verify(token, privateKey);
    if (decoded && decoded.iss === code) {
      Admin.findOne({where:userinfo}).then((result,err) =>{
        if(result != null){
          res.cookie('adminName',result.adminName);
          res.cookie('adminID',result.adminID);
          res.json({code: 0, msg: "登录成功"});
        }else{
          res.json({code: 1, msg: "验证失败"});
        }
      });
    }else{
      res.json({code: 1, msg: "验证码错误！"});
    }
  } catch (err) {
    res.json({code: 1,msg: err.message});
    return false;
  }
  });

//管理员手机号等录
router.post("/adminPhoneLogin", async (req,res,next) => {
  const { body } = req;
  console.log(body)
  const { token, code } = body;
  const userinfo = {
    adminPhone:req.body.phone_num,
  };
  try {
    const decoded = jwt.verify(token, privateKey);
    if (decoded && decoded.iss === code) {
      //添加数据库操作
      const result = await Admin.findOne({where:userinfo});
      if(result != null){
        console.log(result);
        // res.cookie('username',result.userName,{maxAge:3600000});  //设置cookie
        res.cookie('adminName',result.adminName);
        res.cookie('adminID',result.adminID);
        result.userPassword='';
        res.json({code: 0, msg: "登陆成功！", user:result});
      }else {
        res.json({code: 1, msg: "查询无记录,登陆失败！"});}
      return;
    }
    res.json({code: 1, msg: "验证码错误！"});
  } catch (err) {
    res.json({code: 1, msg: err.message});
  }
});

module.exports = router;