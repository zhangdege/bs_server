const express = require('express');
const router = express.Router();
const {Comment} = require('../DB_config/Schemas/comment.js');
const {User} = require('../DB_config/Schemas/user.js');
const {Collect} = require('../DB_config/Schemas/collect.js');
const dt = require('silly-datetime');


//收藏记录
router.post('/saveCollect', async (req, res, next) => {
    console.log(req.body)
    let data = {};
    let {articleID, userid, AgreetTime, type} = req.body;
    data.ObjectID = articleID;
    data.userID = userid;
    data.type = type;
    data.CollectTime = AgreetTime;
    try {
        if(await Collect.findOne({where:{ObjectID:data.ObjectID,type:data.type,userID:data.userID}})){
            res.json({code: 1, msg: '你已经收藏过了，换个试试吧！'})
        }else{
            await Collect.create(data).then(result => {
                res.json({code: 0, msg: '收藏成功！'})
            })
        }
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//收藏下详情以及数目获取
router.post('/getCollect', async (req, res, next) => {
    let data = {};
    let {type, articleID} = req.body;
    data.type = type;
    data.ObjectID = articleID;
    try {
        await Collect.findAll({
            where: data
        }).then(result => {
            res.json({code: 0, msg: '获取成功！', data: result});
        })
    } catch (e) {
        res.json({code: 1, msg: e.message});
    }
});

//删除收藏
router.post('/delete',async (req,res,next)=>{
    let {collectID} = req.body;
    try{
        await Collect.destroy({where:{collectID:collectID}}).then((success,error)=>{
            success?res.json({code:0,msg:'删除成功!'}):res.json({code:1,msg:error})
        });

    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});

module.exports = router;