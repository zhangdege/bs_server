const express = require('express');
const router = express.Router();
const {Comment} = require('../DB_config/Schemas/comment.js');
const {User} = require('../DB_config/Schemas/user.js');
const {Collect} = require('../DB_config/Schemas/collect.js');
const {Article} = require('../DB_config/Schemas/article');
const {ArtCg} = require('../DB_config/Schemas/bs_article_caogao.js');
const {Video} = require('../DB_config/Schemas/SCVideo.js');
const {Dianzan} = require('../DB_config/Schemas/dianzan.js');
const {Share} = require('../DB_config/Schemas/share.js');
const {DisAgree} = require('../DB_config/Schemas/disagree.js');
const dt = require('silly-datetime');

//全局定义查找条件
const finder_limiter = {};
finder_limiter.isDelete = 0;
finder_limiter.isPassVerify = 1;

//个人管理页面获取数据
router.post('/getUserAllData',async (req,res,next)=>{
    let videoData = {};
    let articleData = {};
    let commentData = {};
    let collectData = {};
    let caogaoData  = {};
    let verify_article_data ={};
    let verify_video_data = {};
    verify_article_data.userId =req.body.userid;
    verify_article_data.isPassVerify=0;
    verify_video_data.userId = req.body.userid;
    verify_video_data.isPassVerify=0;
    verify_video_data.isDelete = 0;
    verify_article_data.isDelete = 0;
    commentData.userID = req.body.userid;
    collectData.userID = req.body.userid;
    caogaoData.userId = req.body.userid;
    videoData.userId = req.body.userid;
    articleData.userId = req.body.userid;
    videoData.isPassVerify =1;  //测试
    articleData.isPassVerify =1; //测试数据
    videoData.isDelete = 0;
    articleData.isDelete = 0;
    Collect.belongsTo(Article, {foreignKey: 'ObjectID', targetKey: 'id'});
    Collect.belongsTo(Video, {foreignKey: 'ObjectID', targetKey: 'id'});
    Comment.belongsTo(Article, {foreignKey: 'ObjectID', targetKey: 'id'});
    Comment.belongsTo(Video, {foreignKey: 'ObjectID', targetKey: 'id'});
    try{
        let vData = await Video.findAll({where:videoData});
        let aData = await Article.findAll({where:articleData});
        let collect = await Collect.findAll({
            include:{
                model:Article,
                attributes:["sectionName","articleTitle"]
            },
            where:{type:'A',...collectData} //解构
        });
        let video_collect = await Collect.findAll({
            include:{
                model:Video,
                attributes:['sectionName','title']
            },
            where:{type:"V",...collectData}   //解构
        });
        collect.push(...video_collect);  //解构video   收藏数据结果
        //获取评论
        let comment = await Comment.findAll({
            include:{
                model:Article,
                attributes:["sectionName","articleTitle"]
            },
            where:{type:'A',...commentData}
        });
        let video_comment = await Comment.findAll({
            include:{
                model:Video,
                attributes:['sectionName','title']
            },
            where:{type:'V',...commentData}
        });
        comment.push(...video_comment);  //评论总数居
        let caogao  = await ArtCg.findAll({where:caogaoData});
        let verifyArticle = await Article.findAll({where:verify_article_data});
        let verifyVideo = await Video.findAll({where:verify_video_data});
        if(vData.length != 0 || aData.length != 0|| collect.length != 0|| comment.length != 0|| caogao.length != 0|| verifyArticle.length != 0||verifyVideo.length != 0){
            res.json({code:0,msg:'获取数据成功！',vData,aData,collect,comment,caogao,verifyArticle,verifyVideo})
        }else{
            res.json({code:1,msg:'获取数据失败！'})
        }
    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});

//个人管理页面删除文章
router.post('/del_article',async (req,res,next)=>{
    let delData = {};
    let sourceData = {};
    delData.id = req.body.id;
    delData.userId = req.body.userid;
    delData.sectionName = req.body.sectionName;
    sourceData.id = req.body.id;
    sourceData.userId = req.body.userid;
    sourceData.sectionName = req.body.sectionName;//sectionName
    delData.isDelete = 1;
    try{
        await Article.update(delData,{where:sourceData}).then(result =>{
            result==null?res.json({code:1,msg:'删除失败！'}):res.json({code:0,msg:"删除成功！"});
        })
    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});

//个人管理页面删除视频
router.post('/del_video',async (req,res,next)=>{
    let delData = {};
    let sourceData = {};
    delData.id = req.body.id;
    delData.userId = req.body.userid;
    delData.sectionName = req.body.sectionName;
    sourceData.id = req.body.id;
    sourceData.userId = req.body.userid;
    sourceData.sectionName = req.body.sectionName;
    delData.isDelete = 1;
    try{
        await Video.update(delData,{where:sourceData}).then(result =>{
            result==null?res.json({code:1,msg:'删除失败！'}):res.json({code:0,msg:"删除成功！"});
        })
    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});

//个人管理页面charts获取数据
router.post('/getAllChartsData',async (req,res,next)=>{
    let videoData = {};
    let articleData = {};
    videoData.userId = req.body.userid;
    articleData.userId = req.body.userid;
    videoData.isPassVerify = 1;  //测试
    articleData.isPassVerify = 1; //测试数据
    videoData.isDelete = 0;
    articleData.isDelete = 0;
    try{
        let vOData = await Video.findAll({
            attributes:["userId","title",'releaseTime'],
            where:videoData
        });
        let aOData = await Article.findAll({
            attributes:["userId","articleTitle",'releaseTime'],
            where:articleData
        });
        // console.log(dt.format('2021-03-16T05:27:19.000Z','MM'));
        if(vOData != null && aOData!= null){
         //遍历数组格式化时间为几月
         //   文章数组
            let aData=[];
            let vData=[];
            aOData.map((item,index)=>{
                let obj = {};
                obj.articleTitle = item.articleTitle;
                obj.userId = item.userId;
                obj.Time = dt.format(item.releaseTime,"YYYY-MM");
                aData.push(obj)
            });
            vOData.map((item,index)=>{
                let obj = {};
                obj.title = item.title;
                obj.userId = item.userId;
                obj.Time = dt.format(item.releaseTime,'YYYY-MM');
                vData.push(obj)
            });
            res.json({code:0,msg:'获取数据成功！',vData,aData})
        }else{
            res.json({code:1,msg:'获取数据失败！'})
        }
    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});

//获取热榜数据
router.get('/today_hot',async (req,res,next)=>{
    try{
        let needData = [];
        let new_obj=[];
        let zan_more_than_one = [];
        let zan_more_than_one_video = [];
        let zan_more_than_one_article = [];
        let result = await Dianzan.findAll();
        let gruop_data = GroupBy(result,(item)=>{
            return [item.ObjectID]
        });
        // 统计哪个Id的点赞数  例如：{Object：3，length：10}  ObjectId是作品Id，type是文章还是视频，length是数量
        gruop_data.map((item,index) =>{
            let obj = {};
            obj.ObjectID = item[0].ObjectID;
            obj.type = item[0].type;
            obj.length = item.length;
            new_obj.push(obj);
        });
        needData.push(...new_obj);
        //开始筛查最多点赞的文章以及视频
        needData.map(item =>{
            if(item.length >1){                     //暂时以1为标准
                zan_more_than_one.push(item)
            }
        });
        //分类视频或者文章
        zan_more_than_one.map(item=>{
            if(item.type=='V'){
                zan_more_than_one_video.push(item)
            }else {
                zan_more_than_one_article.push(item)
            }
        });
        //开始各自搜索并返回前1条数据
        let target_one = await Article.findOne({where:{id:zan_more_than_one_article[0].ObjectID}});
        let tartget_two = await Video.findOne({where:{id:zan_more_than_one_video[0].ObjectID}});
        let target_data = [];
        target_data.push(tartget_two);
        target_data.push(target_one);
        //返回数据
        res.json({code:0,data:target_data});
    }catch (e) {
        res.json({code:1,msg:e.message})
    }
});

// 推荐数据分类方法
function GroupBy(array, fn) {
    const groups = {};
    array.forEach(function (item) {
        const group = JSON.stringify(fn(item));
        //这里利用对象的key值唯一性的，创建数组
        groups[group] = groups[group] || [];
        groups[group].push(item);
    });
    //最后再利用map循环处理分组出来
    return Object.keys(groups).map(function (group) {
        return groups[group];
    });
}
// 获取热榜数据结束标志 ----、、、、、|||||||||

//搜索数据
router.get('/search',async (req,res,next)=>{
    try{
        let article_data = await Article.findAll({where:{...finder_limiter}});
        let video_data = await Video.findAll({where:{...finder_limiter}});
        let re_Data = [];
        //处理文章数据 成 {title:'',id:'',author:'',type:''}
        article_data.map(item => {
            let object = {};
            object.value = item.articleTitle;
            object.id = item.id;
            object.sectionName = item.sectionName;
            object.author = item.userName;
            object.type = "A";
            re_Data.push(object);
        });
        //处理视频数据 成 {title:'',id:'',author:'',type:''}
        video_data.map(item => {
            let object = {};
            object.value = item.title;
            object.id = item.id;
            object.sectionName = item.sectionName;
            object.author = item.userName;
            object.type = "V";
            re_Data.push(object);
        });
        res.json({code:0,msg:'success get Data',data:re_Data});
    }catch (e) {
        res.json({code:1,mag:e.message});
    }
});

//userMain页面数据
router.post('/getUserMainInfo',async (req,res,next)=>{
    let {userName} = req.body;
    let article_data = {};
    let video_data = {};
    article_data.userName = userName;
    article_data.isPassVerify = 1;
    article_data.isDelete = 0;
    article_data.isRelease = 1;
    video_data.userName = userName;
    video_data.isPassVerify = 1;
    video_data.isDelete = 0;
    try{
        let userInfo = await User.findOne({where:{userName:userName}});
        let video = await Video.findAll({where:video_data});
        let article = await Article.findAll({where:article_data});
        //返回数据给前端
        res.json({code:0,msg:'获取用户数据成功！',video,article,userInfo});
    }catch (e) {
        res.json({code:1,msg:e.message});
    }
});
module.exports = router;