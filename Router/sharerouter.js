const express = require('express');
const router = express.Router();
const {Comment} = require('../DB_config/Schemas/comment.js');
const {User} = require('../DB_config/Schemas/user.js');
const {Share} = require('../DB_config/Schemas/share.js');
const dt = require('silly-datetime');


//分享记录保存
router.post('/saveShare', async (req, res, next) => {
    let data = {};
    let {articleID, userid, AgreetTime, type} = req.body;
    data.ObjectID = articleID;
    data.userID = userid;
    data.type = type;
    data.ShareTime = AgreetTime;
    try {
        await Share.create(data).then(result => {
            res.json({code: 0, msg: '分享成功！'})
        })
    } catch (e) {
        res.json({code: 1, msg: e.message})
    }
});

//分享详情获取
router.post('/getShare', async (req, res, next) => {
    let data = {};
    let {type, articleID} = req.body;
    data.type = type;
    data.ObjectID = articleID;
    try {
        await Share.findAll({
            where: data
        }).then(result => {
            res.json({code: 0, msg: '获取成功！', data: result});
        })
    } catch (e) {
        res.json({code: 1, msg: e.message});
    }
});


module.exports = router;