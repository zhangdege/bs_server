var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require("cors");
const mkdirp = require("mkdirp");
mkdirp.sync('./Upload/'); //判断目录是否存在，不存在则创建

// 引入路由
const ArticleRouter = require("./Router/ArticleRouter.js");
const UserRouter = require("./Router/UserRouter.js");
const ApiRouter = require("./Router/Api.js");
const VideoRouter = require('./Router/Video.js');
const superRouter = require("./Router/superUser.js");
const avatar = require("./Router/avatar.js");
const Comment = require("./Router/comment.js");
// const share = require("./Router/sharerouter.js");  //不做分享
const collect = require('./Router/collect.js');
const diznan = require("./Router/dianzan.js");
const disAgree = require("./Router/disAgree.js");
const indexRouter = require("./Router/index.js");
const admin = require('./Router/admin.js');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/photo', express.static(path.join(__dirname, 'Upload')));
app.use('/video', express.static(path.join(__dirname, 'video')));
app.use('/avatar', express.static(path.join(__dirname, 'avatar')));

//设置跨域请求客户端
const whitelist = [
    'http://localhost:8080',
    "http://localhost:3000",
    'http://localhost:8081',
    "http://localhost:8084",
    "http://localhost:8083",
    "http://localhost:8082",
    "*"];
const corsOptions = {
    credentials: true, // This is very important.
    origin: (origin, callback) => {
        if (whitelist.includes(origin))
            return callback(null, true);
        callback(new Error('Not allowed by CORS'));
    }
};
app.use(cors(corsOptions));
// app.use(cors());

app.use('/', ArticleRouter);
app.use('/users', UserRouter);
app.use('/api', ApiRouter);
app.use('/superUser', superRouter);
app.use('/video', VideoRouter);
app.use('/avatar', avatar);
app.use('/comment', Comment);
// app.use('/share', share);
app.use('/dianzan', diznan);
app.use('/collect', collect);
app.use('/disagree',disAgree);
app.use('/index',indexRouter);
app.use('/admin',admin);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;