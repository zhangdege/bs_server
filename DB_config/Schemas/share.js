const {Sequelize, Model, DataTypes} = require("sequelize");
const {db} = require("../sequelize.js");
const Share = db.define('share', {  //admin表
        shareID: {                             //id
            type: DataTypes.INTEGER,
            unique: true,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        ObjectID: {                   //作品id
            type: DataTypes.INTEGER,
            allowNull: false
        },
        userID: {                   //用户id
            type: DataTypes.INTEGER,
            allowNull: false
        },
        ObjectUrl: {
            type: DataTypes.STRING,
            allowNull: false
        },
        ShareTime: {
            type: DataTypes.DATE,
            allowNull: false
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false
        },
    },
    {
        timestamps: false
    },
    {
        tableName: 'share'
    });
module.exports = {Share};