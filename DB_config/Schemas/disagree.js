const {Sequelize, Model, DataTypes} = require("sequelize");
const {db} = require("../sequelize.js");
const DisAgree = db.define('disagree', {  //admin表
        disagreeID: {                             //id
            type: DataTypes.INTEGER,
            unique: true,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        ObjectID: {                   //作品id
            type: DataTypes.INTEGER,
            allowNull: false
        },
        userID: {                   //用户密码
            type: DataTypes.INTEGER,
            allowNull: false
        },
        disAgreeTime: {
            type: DataTypes.DATE,
            allowNull: false
        },
        type: {
            type: DataTypes.STRING,
            allowNull: false
        },
    },
    {
        timestamps: false
    },
    {
        tableName: 'DisAgree'
    });
module.exports = {DisAgree};