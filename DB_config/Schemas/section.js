/*
 * @Author: your name
 * @Date: 2020-12-17 21:55:02
 * @LastEditTime: 2020-12-17 21:55:03
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \BS_毕业设计\nodejs\Blog_of_mine\DB_config\Schemas\section.js
 */
const {Sequelize, Model, DataTypes} = require("sequelize");
const {db} = require("../sequelize.js");

const Section = db.define('section',{
    sectionId:{
        type:DataTypes.INTEGER,
        unique: true,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    sectionName:{
        type:DataTypes.STRING(5000),
        allowNull: false
    }
},
    {
        timestamps: false
    },
    {
        tableName: 'section'
    }
);
module.exports={Section};