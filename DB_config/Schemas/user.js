//  用户表 ,先做大概，后面再补充，再写触发器
const { Sequelize, Model, DataTypes } = require("sequelize");
const {db}=require("../sequelize.js");
const User = db.define('User',{  //user表
    userID:{                             //用户id
        type:DataTypes.INTEGER,
        unique:true,
        autoIncrement:true,
        primaryKey: true,
        allowNull:false
    },
    userName:{                   //用户名
        default:"user"+new Date().getTime(),
        type:DataTypes.STRING,
        allowNull:false
    },
    userPassword:{                   //用户密码
        type:DataTypes.STRING,
        allowNull:false
    },
    userPhone:{
        type:DataTypes.STRING,
        allowNull:false
    },
    userSex:{
        type:DataTypes.STRING,
        allowNull:true
    },
     userAvator:{
        type:DataTypes.STRING,
        default:"/photo/avator.png",
        allowNull:true
    },
    userEmail:{
        type:DataTypes.STRING,
        allowNull:true
    },
    userBirthday:{
        type:DataTypes.DATE(6),
        default:Sequelize.now,
        allowNull:true
    },
    userStatement:{
        type:DataTypes.STRING,
        default:"这个家伙很懒，什么都没有留下。",
        allowNull:true
    },
    userRegDate:{                 //注册时间
        type:DataTypes.DATE(6),
        allowNull:true
    },
    userLastLogin:{              //最近登录
        type:DataTypes.DATE(6),
        default:Sequelize.now,
        allowNull:true
    },
    isLimit:{
        type:DataTypes.INTEGER,
    },
},
{
    timestamps: false
},
{
    tableName: 'users'
});
module.exports={User};