const { Sequelize, Model, DataTypes } = require("sequelize");
const {db}=require("../sequelize.js");
const Dianzan = db.define('dianzan',{  //admin表
        dianzanID:{                             //id
            type:DataTypes.INTEGER,
            unique:true,
            autoIncrement:true,
            primaryKey: true,
            allowNull:false
        },
        ObjectID:{                   //作品id
            type:DataTypes.INTEGER,
            allowNull:false
        },
        userID:{                   //用户密码
            type:DataTypes.INTEGER,
            allowNull:false
        },
        AgreeTime:{
            type:DataTypes.DATE,
            allowNull:false
        },
        type:{
            type:DataTypes.STRING,
            allowNull:false
        },
    },
    {
        timestamps: false
    },
    {
        tableName: 'dianzan'
    });
module.exports={Dianzan};