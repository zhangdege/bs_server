const {Sequelize, Model, DataTypes} = require("sequelize");
const {db} = require("../sequelize.js");

const Video = db.define('video', {
        id: {
            type: DataTypes.INTEGER,
            unique: true,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        userName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        userId: {
            type: DataTypes.INTEGER
        },
        sectionName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        title: {               //主题
            type: DataTypes.STRING,
            allowNull: false
        },
        videoUrl: {
            type: DataTypes.STRING,
            allowNull: false
        },
        videoName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        pictureurl: {
            type: DataTypes.STRING,
            allowNull: false
        },
        pictureName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        attribute: {
            type: DataTypes.STRING,
            allowNull: false
        },
        releaseTime: {
            type: DataTypes.DATE(6),
        },
        modifyTime: {
            type: DataTypes.DATE(6),
        },
        isRefuse: {
            type: DataTypes.INTEGER
        },
        isPassVerify: {
            type: DataTypes.INTEGER
        },
        isDelete: {
            type: DataTypes.INTEGER,
            default:0
        }
    },
    {
        timestamps: false
    },
    {
        tableName: 'video'
    });
module.exports = {Video};