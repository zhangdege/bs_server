/*
 * @Author: your name
 * @Date: 2020-12-17 21:54:24
 * @LastEditTime: 2020-12-17 21:54:25
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \BS_毕业设计\nodejs\Blog_of_mine\DB_config\Schemas\admin.js
 */
const { Sequelize, Model, DataTypes } = require("sequelize");
const {db}=require("../sequelize.js");
const Admin = db.define('admin',{  //admin表
        adminID:{                             //用户id
            type:DataTypes.INTEGER,
            unique:true,
            autoIncrement:true,
            primaryKey: true,
            allowNull:false
        },
        adminName:{                   //用户名
            default:"user"+new Date().getTime(),
            type:DataTypes.STRING,
            allowNull:false
        },
        adminPassword:{                   //用户密码
            type:DataTypes.STRING,
            allowNull:false
        },
        adminPhone:{
            type:DataTypes.STRING,
            allowNull:false
        },
        adminAvator:{
            type:DataTypes.STRING,
            default:"/photo/avator.png",
            allowNull:true
        },
        adminEmail:{
            type:DataTypes.STRING,
            allowNull:true
        },
        adminBirthday:{
            type:DataTypes.DATE(6),
            default:Sequelize.now,
            allowNull:true
        },
        adminStatement:{
            type:DataTypes.STRING,
            default:"这个家伙很懒，什么都没有留下。",
            allowNull:true
        },
        adminRegDate:{                 //注册时间
            type:DataTypes.DATE(6),
            allowNull:true
        },
        adminLastLogin:{              //最近登录
            type:DataTypes.DATE(6),
            default:Sequelize.now,
            allowNull:true
        },
        isLimit:{
            type:DataTypes.INTEGER,
            allowNull:true
        },
    },
    {
        timestamps: false
    },
    {
        tableName: 'admin'
    });
module.exports={Admin};