const { Sequelize, Model, DataTypes } = require("sequelize");
const {db}=require("../sequelize.js");
const Comment = db.define('comment',{  //admin表
        commentID:{                             //id
            type:DataTypes.INTEGER,
            unique:true,
            autoIncrement:true,
            primaryKey: true,
            allowNull:false
        },
        ObjectID:{                   //作品id
            type:DataTypes.INTEGER,
            allowNull:false
        },
        userID:{                   //用户密码
            type:DataTypes.INTEGER,
            allowNull:false
        },
        conment_centent:{
            type:DataTypes.STRING,
            allowNull:false
        },
        CommentTime:{
            type:DataTypes.DATE,
            allowNull:false
        },
        type:{
            type:DataTypes.STRING,
            allowNull:false
        },
        isPassverify:{
            type:DataTypes.INTEGER,
            default:0,
            allowNull:true
        },
    },
    {
        timestamps: false
    },
    {
        tableName: 'comment'
    });
module.exports={Comment};