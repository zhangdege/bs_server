const { Sequelize, Model, DataTypes } = require("sequelize");
const {db}=require("../sequelize.js");
const Collect = db.define('collect',{  //admin表
        collectID:{                             //id
            type:DataTypes.INTEGER,
            unique:true,
            autoIncrement:true,
            primaryKey: true,
            allowNull:false
        },
        ObjectID:{                   //作品id
            type:DataTypes.INTEGER,
            allowNull:false
        },
        userID:{                   //用户ID
            type:DataTypes.INTEGER,
            allowNull:false
        },
        CollectTime:{
            type:DataTypes.DATE,
            allowNull:false
        },
        type:{
            type:DataTypes.STRING,
            allowNull:false
        },
    },
    {
        timestamps: false
    },
    {
        tableName: 'collect'
    });
module.exports={Collect};