/*
 * @Author: your name
 * @Date: 2020-12-17 19:52:28
 * @LastEditTime: 2020-12-17 20:17:50
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \BS_毕业设计\nodejs\Blog_of_mine\DB_config\Schemas\article.js
 */
// 文章表
const {Sequelize, Model, DataTypes} = require("sequelize");
const {db} = require("../sequelize.js");

const ArtCg = db.define("bs_article_caogao", {
        id: {                              //id
            type: DataTypes.INTEGER,
            unique: true,
            autoIncrement: true,
            primaryKey: true,
            allowNull: false
        },
        articleTitle: {               //主题
            type: DataTypes.STRING,
            allowNull: false
        },
        articleContent: {             //内容
            type: DataTypes.STRING,
            allowNull: false
        },
        articleMdSource: {    //md内容
            type: DataTypes.STRING,
            allowNull: false
        },
        releaseTime: {                //发布时间
            type: DataTypes.DATE(6),
            allowNull: true
        },
        userId: {                     //作者id
            type: DataTypes.INTEGER
        },
        userName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        sectionName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        updateTime: {            //更新时间
            type: DataTypes.INTEGER
        },
        isRelease: {
            type: DataTypes.INTEGER
        },
        isRefuse: {
            type: DataTypes.INTEGER
        },
        isPassVerify: {
            type: DataTypes.INTEGER
        }

    },
    {
        timestamps: false
    },
    {
        tableName: 'bs_article_caogao'
    }
);


module.exports = {ArtCg};