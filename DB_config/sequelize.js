/*
 * @Author: your name
 * @Date: 2020-11-10 21:07:19
 * @LastEditTime: 2020-11-13 18:51:12
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \nodejs\mine\DB_config\sequelize.js
 */
const { Sequelize } = require("sequelize");
const baseConfig = require('../baseConfig.js');
const db = new Sequelize(
    `${baseConfig.databaseName}`,
    `${baseConfig.databaseUser}`,
    `${baseConfig.databasePassword}`,
    {
        dialect:`${baseConfig.databaseType}`,
        host:`${baseConfig.dataBaseLocation}`
    }
);
// db.authenticate().then(console.log("connect successfully!")).catch(err=>{console.log("连接数据库失败！",err)})  //测试连接状态
module.exports={db};