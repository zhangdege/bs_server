const baseUrl = 'http://localhost:3000'; //运行网址地址
const path =require("path");
module.exports = {
    baseUrl,
    api:`${baseUrl}/api/`,           // api地址
    captcha:path.resolve('./public/images/captcha.png'),  //验证码地址
    photo:`${baseUrl}/photo/`,       //图片访问地址
    video:`${baseUrl}/video/`,       //视频访问地址
    avatar:`${baseUrl}/avatar/`,     //头像访问地址
    static:`${baseUrl}/static/`,     //静态文件地址
    databaseType:'mysql',            //数据库类型
    dataBaseLocation:'127.0.0.1',    //数据库地址
    databaseUser:'zhang',             //数据库用户名
    databasePassword:'123456',       //数据库密码
    databaseName:'bs_log',          //数据库名称
};